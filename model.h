#ifndef MODEL_H
#define MODEL_H

#include <QVector>
#include <QTextStream>

namespace GraphModel{

class Node;
class Edge;

enum AlgorithmType 
{
    NO_ALGORITHM        = -1,
    DECISION_BASED      = 0,
    INDEPENDENT_CASCADE = 1
};

class Model : public QObject
{
    Q_OBJECT
signals:
    void graphCreated(const QVector<GraphModel::Node*>*, const QVector<GraphModel::Edge*>*, bool, bool);
    void graphModified(const QVector<GraphModel::Node*>*, const QVector<GraphModel::Edge*>*);
    void algorithmConverged();
    void errorMessageFromModel(const QString&);

public:
    Model(QObject *parent = nullptr);
    ~Model();

    enum {NO_WEIGHT = 0};

    void createRandomGraph(int nodesCount, double edgeProb, bool directed, bool weighted,
                           double weightsLoverBound, double weightsUpperBound);
    void createGraphFromFile(const QString &fileName);
    bool saveStateIntoFile(QString& fileName) const;
    bool startAlgorithm(GraphModel::AlgorithmType algoType, const QVector<int> &initActiveNodes, double treshold = 0);
    void stepAlgorithm();
    void endAlgorithm();
    bool isDirected() const;
    bool isWeighted() const;
    bool isAlgorithmInProcess() const;
    bool isConverged() const;

private:
    bool                                        _isDirected;
    bool                                        _isWeighted;
    QVector<GraphModel::Node*>                  _nodes;
    QVector<GraphModel::Edge*>                  _edges;
    GraphModel::AlgorithmType                   _currentAlgorithm;
    bool                                        _isAlgorithmInProcess;
    bool                                        _isConverged;
    double                                      _tresholdForDecisionBasedAlgorithm;
    QVector<GraphModel::Node*>                  _previouslyActivatedNodes;
    QVector<std::tuple<int, int, int, double>>  _activatedNodesPerRound;
    int                                         _iterationCount;

    void            deleteEdges(QVector<GraphModel::Edge*>& edges);
    void            deleteNodes(QVector<GraphModel::Node*>& nodes);
    void            deleteGraph(QVector<GraphModel::Edge*>& edges, QVector<GraphModel::Node*>& nodes);
    void            movingThroughCommentAndEmptyLines(QTextStream &in, QString &line, const QString &commentSign);
    void            propogateErrorMessageAndDeleteGraph(const QString& msg,
                                                        QVector<GraphModel::Edge*>* edges = nullptr,
                                                        QVector<GraphModel::Node*>* nodes = nullptr);
    QPair<bool,int> checkReverseEdgeExistence(GraphModel::Node* const src, GraphModel::Node* const dst,
                                              QVector<GraphModel::Edge*> const *edges) const;
    void            addNewOrUpdateExistingEdgeToBidirectional(GraphModel::Node* src, GraphModel::Node* dst,
                                                              QVector<GraphModel::Edge*> *edges,
                                                              double weight);
    void            addNewOrUpdateExistingEdgeToBidirectionalDuringFileReading(GraphModel::Node* src,
                                                                               GraphModel::Node* dst,
                                                                               QVector<GraphModel::Edge*> *edges,
                                                                               double weight,
                                                                               bool isDirected);
    void            takeCareModelStateWhenLeavingAlgorithmMode();
};

}

#endif // MODEL_H

#include <QKeyEvent>
#include <QRandomGenerator>
#include <QColorDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QMainWindow>
#include <QFileDialog>

#include "graphwidget.h"
#include "networkcreatedialog.h"
#include "algorithmpickerdialog.h"
#include "modelnode.h"
#include "modeledge.h"

GraphView::GraphWidget::GraphWidget(QWidget &parent, GraphModel::Model &model)
    : QGraphicsView(&parent), _model(model), _mainMenuBar(*(static_cast<QMainWindow*>(&parent)->menuBar())),
      _backgroundColorTopGradient(Qt::white), _backgroundColorBottomGradient(Qt::lightGray), _timerId(0)
{
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    scene->setSceneRect(0, 0, 400, 400);
    setScene(scene);

    setCacheMode(CacheNone);
    setViewportUpdateMode(ViewportUpdateMode::FullViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    scale(qreal(1), qreal(1));
    setMinimumSize(800, 600);

    connect(&model,
            SIGNAL(graphCreated(const QVector<GraphModel::Node*>*, const QVector<GraphModel::Edge*>*, bool, bool)),
            this,
            SLOT(graphCreated(const QVector<GraphModel::Node*>*, const QVector<GraphModel::Edge*>*, bool, bool)));

    connect(&model,
            SIGNAL(graphModified(const QVector<GraphModel::Node*>*, const QVector<GraphModel::Edge*>*)),
            this,
            SLOT(graphModified(const QVector<GraphModel::Node*>*, const QVector<GraphModel::Edge*>*)));

    connect(&model, SIGNAL(errorMessageFromModel(const QString&)), this, SLOT(showMessage(const QString&)));

    createActions();
    createMenus();

    model.createRandomGraph(10, 1.0, true, false, GraphModel::Model::NO_WEIGHT, GraphModel::Model::NO_WEIGHT);
}

void GraphView::GraphWidget::graphCreated(const QVector<GraphModel::Node*> *modelNodes,
                                          const QVector<GraphModel::Edge*> *modelEdges,
                                          bool isDirected, bool isWeighted)
{
    deleteNodes();
    deleteEdges();

    if(modelNodes != nullptr)
    {
        _nodes.resize(modelNodes->size());
        for(int i = 0; i < _nodes.size(); ++i)
        {
            _nodes[i] = new GraphView::Node(*this, (*modelNodes)[i]->getId());
            scene()->addItem(_nodes[i]);
        }
    }

    if(modelEdges != nullptr)
    {
        _edges.resize(modelEdges->size());
        for(int i = 0; i < _edges.size(); ++i)
        {
            _edges[i] = new GraphView::Edge(*(_nodes[(*modelEdges)[i]->getSourceNode()->getId()-1]),
                                            *(_nodes[(*modelEdges)[i]->getDestinationNode()->getId()-1]),
                                            isWeighted,
                                            (*modelEdges)[i]->getSourceToDestinationWeight(),
                                            isDirected);

            if((*modelEdges)[i]->isBidirectional())
            {
                _edges[i]->setSymmetric(true);
                _edges[i]->setDestinationToSourceWeight((*modelEdges)[i]->getDestinationToSourceWeight());
            }

            scene()->addItem(_edges[i]);
        }
    }

    shuffle();

    _setNodeForcesAction->setChecked(false);
    _setNodeShadowAction->setChecked(false);

    _saveGraphStateAction->setEnabled(false);

    _stepAlgorithmAction->setEnabled(false);
    _leaveAlgorithmAction->setEnabled(false);
}

void GraphView::GraphWidget::graphModified(const QVector<GraphModel::Node*> *modelNodes,
                                           const QVector<GraphModel::Edge*> *modelEdges)
{
    if(modelNodes != nullptr)
    {
        for(int i = 0; i < _nodes.size(); ++i)
        {
            _nodes[i]->setActive((*modelNodes)[i]->isActive());
            _nodes[i]->update();
        }
    }

    if(modelEdges != nullptr)
    {
        _edges.resize(modelEdges->size());
        for(int i = 0; i < _edges.size(); ++i)
        {
            _edges[i]->setActive((*modelEdges)[i]->isActive());
            _edges[i]->update();
        }
    }

    resetCachedContent();
}

void GraphView::GraphWidget::showMessage(const QString &msg)
{
    QMessageBox msgBox;
    msgBox.setText(msg);
    msgBox.exec();
}

void GraphView::GraphWidget::createActions()
{
    _createRandomGraphAction = new QAction(tr("&Create random graph"), this);
    _createRandomGraphAction->setShortcut(Qt::Key_C);
    _createRandomGraphAction->setStatusTip(tr("Create a directed/undirected, weighted/unweighted network"));
    connect(_createRandomGraphAction, &QAction::triggered, this, &GraphWidget::createRandomGraph);

    _readGraphFromFileAction = new QAction(tr("&Read graph from file"), this);
    _readGraphFromFileAction->setShortcut(Qt::Key_R);
    _readGraphFromFileAction->setStatusTip(tr("Read a directed/undirected, weighted/unweighted network from file"));
    connect(_readGraphFromFileAction, &QAction::triggered, this, &GraphWidget::readGraphFromFile);

    _saveGraphStateAction = new QAction(tr("Save State of Graph"));
    _saveGraphStateAction->setShortcut(QKeySequence::Save);
    _saveGraphStateAction->setStatusTip(tr("Save the current state of the graph into a file"));
    connect(_saveGraphStateAction, &QAction::triggered, this, &GraphWidget::saveStateOfGraph);
    _saveGraphStateAction->setEnabled(false);

    _setEdgeColorActiveAction = new QAction(tr("Active color"));
    _setEdgeColorActiveAction->setStatusTip(tr("Color of edges that became active during an algorithm"));
    connect(_setEdgeColorActiveAction, &QAction::triggered, this, &GraphWidget::setEdgeColor);

    _setEdgeColorInactiveAction = new QAction(tr("Inactive color"));
    _setEdgeColorInactiveAction->setStatusTip(tr("Color of the still inactive edges during an algorithm"));
    connect(_setEdgeColorInactiveAction, &QAction::triggered, this, &GraphWidget::setEdgeColor);

    _setArrowSizeAction = new QAction(tr("Arrows size"));
    _setArrowSizeAction->setShortcut(Qt::Key_A);
    _setArrowSizeAction->setStatusTip(tr("Size of the arrows located ends on the edges"));
    connect(_setArrowSizeAction, &QAction::triggered, this, &GraphWidget::setArrowSize);

    _setNodeColorActiveTopGradientAction = new QAction(tr("Active start color"));
    _setNodeColorActiveTopGradientAction->setStatusTip(tr("Start color of the colortransition of active nodes"));
    connect(_setNodeColorActiveTopGradientAction, &QAction::triggered, this, &GraphWidget::setNodeColor);

    _setNodeColorActiveBottomGradientAction = new QAction(tr("Active end color"));
    _setNodeColorActiveBottomGradientAction->setStatusTip(tr("End color of the colortransition of active nodes"));
    connect(_setNodeColorActiveBottomGradientAction, &QAction::triggered, this, &GraphWidget::setNodeColor);

    _setNodeColorInactiveTopGradientAction = new QAction(tr("Inactive start color"));
    _setNodeColorInactiveTopGradientAction->setStatusTip(tr("Start color of the colortransition of inactive nodes"));
    connect(_setNodeColorInactiveTopGradientAction, &QAction::triggered, this, &GraphWidget::setNodeColor);

    _setNodeColorInactiveBottomGradientAction = new QAction(tr("Inactive end color"));
    _setNodeColorInactiveBottomGradientAction->setStatusTip(tr("End color of the colortransition of inactive nodes"));
    connect(_setNodeColorInactiveBottomGradientAction, &QAction::triggered, this, &GraphWidget::setNodeColor);

    _setNodeShadowAction = new QAction(tr("Shadow"));
    _setNodeShadowAction->setShortcut(Qt::Key_S);
    _setNodeShadowAction->setCheckable(true);
    _setNodeShadowAction->setStatusTip(tr("Shows shadows of the nodes"));
    connect(_setNodeShadowAction, &QAction::triggered, this, &GraphWidget::setNodeShadow);

    _setNodeForcesAction = new QAction(tr("Forces"));
    _setNodeForcesAction->setShortcut(Qt::Key_F);
    _setNodeForcesAction->setCheckable(true);
    _setNodeForcesAction->setStatusTip(tr("Simulate physic forces between nodes"));
    connect(_setNodeForcesAction, &QAction::triggered, this, &GraphWidget::setNodeForces);

    _shuffleAction = new QAction(tr("Shuffle"));
    _shuffleAction->setShortcut(Qt::Key_Space);
    _shuffleAction->setStatusTip(tr("Randomize all nodes on the scene"));
    connect(_shuffleAction, &QAction::triggered, this, &GraphWidget::shuffle);

    _setBackgroundColorTopGradientAction = new QAction(tr("Start color"));
    _setBackgroundColorTopGradientAction->setStatusTip(tr("Start color of the colortransition of background scene"));
    connect(_setBackgroundColorTopGradientAction, &QAction::triggered, this, &GraphWidget::setBackgroundColor);

    _setBackgroundColorBottomGradientAction = new QAction(tr("End color"));
    _setBackgroundColorBottomGradientAction->setStatusTip(tr("End color of the colortransition of background scene"));
    connect(_setBackgroundColorBottomGradientAction, &QAction::triggered, this, &GraphWidget::setBackgroundColor);

    _setEdgeThicknessAction = new QAction(tr("Edge thickness"));
    _setEdgeThicknessAction->setShortcut(Qt::Key_T);
    _setEdgeThicknessAction->setStatusTip(tr("Thickness size of edges"));
    connect(_setEdgeThicknessAction, &QAction::triggered, this, &GraphWidget::setEdgeThickness);

    _invokeAlgorithmAction = new QAction(tr("Play Algorithm"));
    _invokeAlgorithmAction->setShortcut(Qt::Key_P);
    _invokeAlgorithmAction->setStatusTip(tr("Enter into algorithm mode and "
                                            "play one of the selectable information propogation algorithms"));
    connect(_invokeAlgorithmAction, &QAction::triggered, this, &GraphWidget::invokeAlgoithmPicker);

    _stepAlgorithmAction = new QAction(tr("Step Foward in Algorithm"));
    _stepAlgorithmAction->setShortcut(Qt::Key_N);
    _stepAlgorithmAction->setStatusTip(tr("Step forward 1 in the iteration to the next state in algorithm mode"));
    connect(_stepAlgorithmAction, &QAction::triggered, this, &GraphWidget::invokeAlgorithmStepper);
    _stepAlgorithmAction->setEnabled(false);

    _leaveAlgorithmAction = new QAction(tr("Leave Algorithm"));
    _leaveAlgorithmAction->setShortcut(Qt::Key_L);
    _leaveAlgorithmAction->setStatusTip(tr("Leave from algorithm mode state"));
    connect(_leaveAlgorithmAction, &QAction::triggered, this, &GraphWidget::invokeAlgorithmLeaver);
    _leaveAlgorithmAction->setEnabled(false);
}

void GraphView::GraphWidget::createMenus()
{
    _networkMenu = _mainMenuBar.addMenu(tr("&Network"));
    _optionsMenu = _mainMenuBar.addMenu(tr("&Options"));

    _edgeSubmenu       = _optionsMenu->addMenu(tr("&Edge"));
    _nodeSubmenu       = _optionsMenu->addMenu(tr("&Node"));
    _backgroundSubmenu = _optionsMenu->addMenu(tr("&Background"));

    _algorithmsMenu = _mainMenuBar.addMenu(tr("&Algorithms"));

    _networkMenu->addSeparator();
    _networkMenu->addAction(_createRandomGraphAction);
    _networkMenu->addAction(_readGraphFromFileAction);
    _networkMenu->addAction(_saveGraphStateAction);

    _edgeSubmenu->addAction(_setEdgeColorActiveAction);
    _edgeSubmenu->addAction(_setEdgeColorInactiveAction);
    _edgeSubmenu->addSeparator();
    _edgeSubmenu->addAction(_setArrowSizeAction);
    _edgeSubmenu->addSeparator();
    _edgeSubmenu->addAction(_setEdgeThicknessAction);

    _nodeSubmenu->addAction(_setNodeColorActiveTopGradientAction);
    _nodeSubmenu->addAction(_setNodeColorActiveBottomGradientAction);
    _nodeSubmenu->addSeparator();
    _nodeSubmenu->addAction(_setNodeColorInactiveTopGradientAction);
    _nodeSubmenu->addAction(_setNodeColorInactiveBottomGradientAction);
    _nodeSubmenu->addSeparator();
    _nodeSubmenu->addAction(_setNodeShadowAction);
    _nodeSubmenu->addAction(_setNodeForcesAction);
    _nodeSubmenu->addAction(_shuffleAction);

    _backgroundSubmenu->addAction(_setBackgroundColorTopGradientAction);
    _backgroundSubmenu->addAction(_setBackgroundColorBottomGradientAction);

    _algorithmsMenu->addAction(_invokeAlgorithmAction);
    _algorithmsMenu->addAction(_stepAlgorithmAction);
    _algorithmsMenu->addAction(_leaveAlgorithmAction);
}

void GraphView::GraphWidget::deleteNodes()
{
    for(GraphView::Node* node : _nodes)
    {
        delete node;
    }
    _nodes.clear();
}

void GraphView::GraphWidget::deleteEdges()
{
    for(GraphView::Edge* edge : _edges)
    {
        delete edge;
    }
    _edges.clear();
}

void GraphView::GraphWidget::readGraphFromFile()
{
    if(anyNodeIsPressingByMouse())
    {
        return;
    }

    QFileDialog dialog(this, "Read Graph");
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setNameFilter(tr("Graph (*.grp)"));
    dialog.setViewMode(QFileDialog::Detail);

    QStringList fileNames;
    if(dialog.exec())
    {
        fileNames = dialog.selectedFiles();
        _model.createGraphFromFile(fileNames[0]);
    }
}

void GraphView::GraphWidget::createRandomGraph()
{
    if(anyNodeIsPressingByMouse())
    {
        return;
    }

    GraphView::NetworkCreateDialog dialog(this);
    int result;

    while((result = dialog.exec()) == QDialog::Accepted &&
          dialog.isWeighted() && dialog.getWeightLoverBound() >= dialog.getWeightUpperBound())
    {
        QMessageBox msgBox(QMessageBox::Critical, tr("Error"), tr("Weights upper bound must be greater "
                                                                  "then weights lover bound!"));
        msgBox.exec();
    }

    if(result == QDialog::Accepted)
    {
        _model.createRandomGraph(dialog.getNodeCount(), dialog.getEdgeProbability(), dialog.isDirected(),
                                 dialog.isWeighted(), dialog.getWeightLoverBound(), dialog.getWeightUpperBound());
    }
}

void GraphView::GraphWidget::saveStateOfGraph()
{
    if(anyNodeIsPressingByMouse() || !_model.isAlgorithmInProcess())
    {
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("State (*.stt)"));

    if(fileName != "")
    {
        if(!_model.saveStateIntoFile(fileName))
        {
            showMessage(tr("Saving current state of graph was not successfull!"));
        }
    }
}

void GraphView::GraphWidget::setEdgeColor()
{
    if(anyNodeIsPressingByMouse())
    {
        return;
    }

    QAction *action = dynamic_cast<QAction*>(sender());
    QColor color    = QColorDialog::getColor();

    if(color.isValid())
    {
        if(action == _setEdgeColorActiveAction)
        {
            for(GraphView::Edge *edge : _edges)
            {
                edge->setColorActive(color);
            }
        }
        else if(action == _setEdgeColorInactiveAction)
        {
            for(GraphView::Edge *edge : _edges)
            {
                edge->setColorInactive(color);
            }
        }

        resetCachedContent();
    }
}

void GraphView::GraphWidget::setArrowSize()
{
    if(anyNodeIsPressingByMouse())
    {
        return;
    }

    bool ok;
    int size = QInputDialog::getInt(this, "Arrow size", "Size:", 10, 1, 30, 1, &ok, Qt::MSWindowsFixedSizeDialogHint);

    if(ok)
    {
        for(GraphView::Edge *edge : _edges)
        {
            edge->setArrowSize(size);
        }
    }
}

void GraphView::GraphWidget::setEdgeThickness()
{
    if(anyNodeIsPressingByMouse())
    {
        return;
    }

    bool ok;
    double size = QInputDialog::getDouble(this, "Edge thickness", "Size:", 0.5, 0.1, 1.0, 2, &ok,
                                          Qt::MSWindowsFixedSizeDialogHint);

    if(ok)
    {
        for(GraphView::Edge *edge : _edges)
        {
            edge->setEdgeThickness(size);
        }
    }
}

void GraphView::GraphWidget::setNodeColor()
{
    if(anyNodeIsPressingByMouse())
    {
        return;
    }

    QAction *action = dynamic_cast<QAction*>(sender());
    QColor color = QColorDialog::getColor();

    if(color.isValid())
    {
        if(action == _setNodeColorActiveTopGradientAction)
        {
            for(GraphView::Node *node : _nodes)
            {
                node->setActiveColorTopGradient(color);
                node->update();
            }
        }
        else if(action == _setNodeColorActiveBottomGradientAction)
        {
            for(GraphView::Node *node : _nodes)
            {
                node->setActiveColorBottomGradient(color);
                node->update();
            }
        }
        else if(action == _setNodeColorInactiveTopGradientAction)
        {
            for(GraphView::Node *node : _nodes)
            {
                node->setInactiveColorTopGradient(color);
                node->update();
            }
        }
        else if(action == _setNodeColorInactiveBottomGradientAction)
        {
            for(GraphView::Node *node : _nodes)
            {
                node->setInactiveColorBottomGradient(color);
                node->update();
            }
        }
    }
}

void GraphView::GraphWidget::setNodeShadow()
{
    QAction *action = dynamic_cast<QAction*>(sender());

    for(GraphView::Node *node : _nodes)
    {
        node->setShadow(action->isChecked());
        node->update();
    }
}

void GraphView::GraphWidget::setBackgroundColor()
{
    if(anyNodeIsPressingByMouse())
    {
        return;
    }

    QAction *action = dynamic_cast<QAction*>(sender());
    QColor color = QColorDialog::getColor();

    if(color.isValid())
    {
        if(action == _setBackgroundColorTopGradientAction)
        {
            _backgroundColorTopGradient = color;
        }
        else if(action == _setBackgroundColorBottomGradientAction)
        {
            _backgroundColorBottomGradient = color;
        }
        resetCachedContent();
    }
}

void GraphView::GraphWidget::setNodeForces()
{
    QAction *action = dynamic_cast<QAction*>(sender());

    for(GraphView::Node *node : _nodes)
    {
        node->setForces(action->isChecked());
        node->calculateForces();
        node->advancePosition();
    }
}

void GraphView::GraphWidget::shuffle()
{
    for(QGraphicsItem *item : scene()->items())
    {
        if(qgraphicsitem_cast<GraphView::Node*>(item))
        {
            item->setPos(QRandomGenerator::global()->bounded(sceneRect().width()),
                         QRandomGenerator::global()->bounded(sceneRect().height()));
        }
    }
}

void GraphView::GraphWidget::invokeAlgoithmPicker()
{
    if(anyNodeIsPressingByMouse())
    {
        return;
    }

    QVector<int> initiallyActiveNodesId(_nodes.size());
    for(int i = 0; i < _nodes.size(); ++i)
    {
        initiallyActiveNodesId[i] = _nodes[i]->getId();
    }

    GraphView::AlgorithmPickerDialog dialog(initiallyActiveNodesId, this);
    int result;

    if((result = dialog.exec()) == QDialog::Accepted)
    {
        dialog.modifyNodesVectorForSelectedNodesId(initiallyActiveNodesId);

        bool successfullyStartedAlgorithm = false;
        if(dialog.getSelectedAlgorithm() == GraphModel::AlgorithmType::DECISION_BASED)
        {
            successfullyStartedAlgorithm = _model.startAlgorithm(GraphModel::AlgorithmType::DECISION_BASED,
                                                                 initiallyActiveNodesId,
                                                                 dialog.getTresholdForDecisionBasedAlgorithm());
        }
        else if(dialog.getSelectedAlgorithm() == GraphModel::AlgorithmType::INDEPENDENT_CASCADE)
        {
            successfullyStartedAlgorithm = _model.startAlgorithm(GraphModel::AlgorithmType::INDEPENDENT_CASCADE,
                                                                 initiallyActiveNodesId);
        }

        if(successfullyStartedAlgorithm)
        {
            _stepAlgorithmAction->setEnabled(true);
            _leaveAlgorithmAction->setEnabled(true);
            _saveGraphStateAction->setEnabled(true);
        }
    }
}

void GraphView::GraphWidget::invokeAlgorithmStepper()
{
    _model.stepAlgorithm();
}

void GraphView::GraphWidget::invokeAlgorithmLeaver()
{
    _model.endAlgorithm();

    _stepAlgorithmAction->setEnabled(false);
    _leaveAlgorithmAction->setEnabled(false);
    _saveGraphStateAction->setEnabled(false);
}

void GraphView::GraphWidget::zoomIn()
{
    scaleView(qreal(1.2));
}

void GraphView::GraphWidget::zoomOut()
{
    scaleView(1 / qreal(1.2));
}

void GraphView::GraphWidget::scaleView(qreal scaleFactor)
{
    qreal factor = transform().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();

    if(factor < 0.90 || factor > 100)
    {
        return;
    }

    scale(scaleFactor, scaleFactor);
}

void GraphView::GraphWidget::itemMoved()
{
    if(!_timerId)
    {
        _timerId = startTimer(1000 / 25);
    }
}

bool GraphView::GraphWidget::anyNodeIsPressingByMouse() const
{
    for(auto node : _nodes)
    {
        if(node->isMousePressing())
        {
            return true;
        }
    }
    return false;
}

void GraphView::GraphWidget::resizeEvent(QResizeEvent *event)
{
    scene()->setSceneRect(0, 0, this->width(), this->height());
    QGraphicsView::resizeEvent(event);
}

#if QT_CONFIG(wheelevent)
void GraphView::GraphWidget::wheelEvent(QWheelEvent *event)
{
    scaleView(pow(static_cast<double>(2), -event->delta() / 960.0));
}
#endif

void GraphView::GraphWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
        case Qt::Key_Plus: zoomIn(); break;
        case Qt::Key_Minus: zoomOut(); break;
        default: QGraphicsView::keyPressEvent(event);
    }
}

void GraphView::GraphWidget::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event)
    QList<GraphView::Node*> moving_nodes;

    for(QGraphicsItem *item : scene()->items())
    {
        if (GraphView::Node *node = qgraphicsitem_cast<GraphView::Node*>(item))
        {
            moving_nodes << node;
        }
    }
    for(GraphView::Node *node : moving_nodes)
    {
        node->calculateForces();
    }

    bool itemsMoved = false;
    for(GraphView::Node *node : moving_nodes)
    {
        if (node->advancePosition())
        {
            itemsMoved = itemsMoved || true;
        }
    }

    if(!itemsMoved)
    {
        killTimer(_timerId);
        _timerId = 0;
    }
}

void GraphView::GraphWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect)

    // Shadow
    QRectF sceneRect = this->sceneRect();
    QRectF rightShadow(sceneRect.right(), sceneRect.top() + 5, 5, sceneRect.height());
    QRectF bottomShadow(sceneRect.left() + 5, sceneRect.bottom(), sceneRect.width(), 5);
    if (rightShadow.intersects(rect) || rightShadow.contains(rect))
    {
        painter->fillRect(rightShadow, Qt::darkGray);
    }
    if (bottomShadow.intersects(rect) || bottomShadow.contains(rect))
    {
        painter->fillRect(bottomShadow, Qt::darkGray);
    }

    // Fill
    QLinearGradient gradient(sceneRect.topLeft(), sceneRect.bottomRight());
    gradient.setColorAt(0, _backgroundColorTopGradient);
    gradient.setColorAt(1, _backgroundColorBottomGradient);
    painter->fillRect(sceneRect, gradient);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(sceneRect);

    if(_model.isAlgorithmInProcess() && _model.isConverged())
    {
        QRectF textRect(sceneRect.left() + 4, sceneRect.top() + 4,
                        sceneRect.width() - 4, sceneRect.height() - 4);
        QString message(tr("Algorithm is in converged state!"));

        QFont font = painter->font();
        font.setBold(true);
        font.setPointSize(14);
        painter->setFont(font);
        painter->setPen(Qt::lightGray);
        painter->drawText(textRect.translated(2, 2), message);
        painter->setPen(Qt::black);
        painter->drawText(textRect, message);
    }
}

GraphView::GraphWidget::~GraphWidget()
{
    deleteNodes();
    deleteEdges();

    delete _readGraphFromFileAction;
    delete _createRandomGraphAction;
    delete _saveGraphStateAction;
    delete _setEdgeColorActiveAction;
    delete _setEdgeColorInactiveAction;
    delete _setArrowSizeAction;
    delete _setNodeColorActiveTopGradientAction;
    delete _setNodeColorActiveBottomGradientAction;
    delete _setNodeColorInactiveTopGradientAction;
    delete _setNodeColorInactiveBottomGradientAction;
    delete _setNodeShadowAction;
    delete _setNodeForcesAction;
    delete _shuffleAction;
    delete _setBackgroundColorTopGradientAction;
    delete _setBackgroundColorBottomGradientAction;
    delete _invokeAlgorithmAction;
    delete _stepAlgorithmAction;
    delete _leaveAlgorithmAction;
    delete _edgeSubmenu;
    delete _nodeSubmenu;
    delete _backgroundSubmenu;
    delete _networkMenu;
    delete _optionsMenu;
    delete _algorithmsMenu;
}

#include <QStyleOption>

#include "graphwidget.h"

GraphView::Node::Node(GraphWidget &graphWidget, int id)
    : _graph(graphWidget), _id(id), _forces(false), _shadow(false), _isActive(false),
    _colorActiveTopGradient(Qt::yellow), _colorActiveBottomGradient(Qt::green),
    _colorInactiveTopGradient(Qt::white), _colorInactiveBottomGradient(Qt::gray), _isMousePressing(false)
{
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setZValue(1);
}

void GraphView::Node::addEdge(Edge *edge)
{
    _edgeList << edge;
    edge->adjust();
}

QList<GraphView::Edge*> GraphView::Node::edges() const
{
    return _edgeList;
}

int GraphView::Node::getId() const
{
    return _id;
}

void GraphView::Node::setForces(bool l)
{
    _forces = l;
}

void GraphView::Node::setShadow(bool l)
{
    _shadow = l;
}

bool GraphView::Node::isActive() const
{
    return _isActive;
}

void GraphView::Node::setActive(bool l)
{
    _isActive = l;
}

void GraphView::Node::setActiveColorTopGradient(QColor color)
{
    _colorActiveTopGradient = color;
}

void GraphView::Node::setActiveColorBottomGradient(QColor color)
{
    _colorActiveBottomGradient = color;
}

void GraphView::Node::setInactiveColorTopGradient(QColor color)
{
    _colorInactiveTopGradient = color;
}

void GraphView::Node::setInactiveColorBottomGradient(QColor color)
{
    _colorInactiveBottomGradient = color;
}

bool GraphView::Node::isMousePressing() const
{
    return _isMousePressing;
}

void GraphView::Node::calculateForces()
{
    if (!scene() || scene()->mouseGrabberItem() == this)
    {
        _newPos = pos();
        return;
    }

    qreal xvel = 0;
    qreal yvel = 0;

    if(_forces)
    {
        for(QGraphicsItem *item : scene()->items())
        {
            Node *node = qgraphicsitem_cast<Node *>(item);
            if (!node)
            {
                continue;
            }

            QPointF vec = mapToItem(node, 0, 0);
            qreal dx = vec.x();
            qreal dy = vec.y();
            double l = 2.0 * (dx * dx + dy * dy);
            if (l > 0)
            {
                xvel += (dx * 150.0) / l;
                yvel += (dy * 150.0) / l;
            }
        }

        double weight = (_edgeList.size() + 1) * 10;
        for(Edge *edge : _edgeList)
        {
            QPointF vec;
            if(&(edge->getSourceNode()) == this)
            {
                vec = mapToItem(&(edge->getDestinationNode()), 0, 0);
            }
            else
            {
                vec = mapToItem(&(edge->getSourceNode()), 0, 0);
            }

            xvel -= vec.x() / weight;
            yvel -= vec.y() / weight;
        }

        if (qAbs(xvel) < 0.1 && qAbs(yvel) < 0.1)
        {
            xvel = yvel = 0;
        }
    }

    QRectF sceneRect = scene()->sceneRect();
    _newPos = pos() + QPointF(xvel, yvel);
    _newPos.setX(qMin(qMax(_newPos.x(), sceneRect.left() + 10), sceneRect.right() - 10));
    _newPos.setY(qMin(qMax(_newPos.y(), sceneRect.top() + 10), sceneRect.bottom() - 10));
}

bool GraphView::Node::advancePosition()
{
    if (_newPos == pos())
    {
        return false;
    }

    setPos(_newPos);
    return true;
}

QRectF GraphView::Node::boundingRect() const
{
    qreal adjust = 2;
    return QRectF( -10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust);
}

QPainterPath GraphView::Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-10, -10, 20, 20);
    return path;
}

void GraphView::Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    if(_shadow)
    {
        painter->setPen(Qt::NoPen);
        painter->setBrush(Qt::darkGray);
        painter->drawEllipse(-7, -7, 20, 20);
    }

    QColor _colorTopGradient, _colorBottomGradient;
    if(_isActive)
    {
        _colorTopGradient = _colorActiveTopGradient;
        _colorBottomGradient = _colorActiveBottomGradient;
    }
    else
    {
        _colorTopGradient = _colorInactiveTopGradient;
        _colorBottomGradient = _colorInactiveBottomGradient;
    }

    QRadialGradient gradient(-3, -3, 10);
    if(option->state & QStyle::State_Sunken)
    {
        gradient.setCenter(3, 3);
        gradient.setFocalPoint(3, 3);
        gradient.setColorAt(1, QColor(_colorTopGradient).light(120));
        gradient.setColorAt(0, QColor(_colorBottomGradient).light(120));
    }
    else
    {
        gradient.setColorAt(0, _colorTopGradient);
        gradient.setColorAt(1, _colorBottomGradient);
    }
    painter->setBrush(gradient);
    painter->setPen(QPen(Qt::black, 0));
    painter->drawEllipse(-10, -10, 20, 20);

    QFont font = painter->font();
    font.setPointSize(6);
    painter->setFont(font);
    painter->setPen(Qt::black);
    painter->drawText(boundingRect(), Qt::AlignCenter, QString::number(_id));
}

QVariant GraphView::Node::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change)
    {
        case ItemPositionHasChanged:
            for (Edge *edge : _edgeList)
            {
                edge->adjust();
            }
            _graph.itemMoved();
            break;
        default:
            break;
    }
    return QGraphicsItem::itemChange(change, value);
}

void GraphView::Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    _isMousePressing = true;
    for (Edge *edge : _edgeList)
    {
        edge->enlarge(2.0);
        edge->setZValue(1);
        edge->adjust();
    }
    update();
    QGraphicsItem::mousePressEvent(event);
}

void GraphView::Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    for (Edge *edge : _edgeList)
    {
        edge->enlarge(0.5);
        edge->setZValue(0);
        edge->adjust();
    }
    update();
    QGraphicsItem::mouseReleaseEvent(event);
    _isMousePressing = false;
}

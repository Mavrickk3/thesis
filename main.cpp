#include <QApplication>
#include <QMainWindow>
#include <QStatusBar>

#include "model.h"
#include "graphwidget.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QMainWindow            mainWindow;
    GraphModel::Model      model;
    GraphView::GraphWidget widget(mainWindow, model);

    mainWindow.setCentralWidget(&widget);
    mainWindow.move(0,0);
    mainWindow.setWindowTitle("Information Spreading Visualiser");
    mainWindow.statusBar()->show();
    mainWindow.show();

    return app.exec();
}

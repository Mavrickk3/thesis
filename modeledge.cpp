#include "modeledge.h"

GraphModel::Edge::Edge(GraphModel::Node* source, GraphModel::Node* destination, double weight)
    : _source(source), _destination(destination), _sourceToDestinationWeight(weight), _isBidirectional(false),
      _isActive(false) {}

GraphModel::Node* GraphModel::Edge::getSourceNode() const
{
    return _source;
}

GraphModel::Node* GraphModel::Edge::getDestinationNode() const
{
    return _destination;
}

double GraphModel::Edge::getSourceToDestinationWeight() const
{
    return _sourceToDestinationWeight;
}

double GraphModel::Edge::getDestinationToSourceWeight() const
{
    return _destinationToSourceWeight;
}

bool GraphModel::Edge::isBidirectional() const
{
    return _isBidirectional;
}

bool GraphModel::Edge::isActive() const
{
    return _isActive;
}

void GraphModel::Edge::activate()
{
    _isActive = true;
}

void GraphModel::Edge::deactivate()
{
    _isActive = false;
}

void GraphModel::Edge::setBidirectional()
{
    _isBidirectional = true;
}

void GraphModel::Edge::setDestinationToSourceWeight(double weight)
{
    _destinationToSourceWeight = weight;
}

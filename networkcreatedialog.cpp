#include "networkcreatedialog.h"

GraphView::NetworkCreateDialog::NetworkCreateDialog(QWidget *parent): QDialog(parent),
  _nodeCount(10), _isDirecetd(false), _isWeighted(false), _edgeProbability(0.5),
  _weightLoverBound(0), _weightUpperBound(1)
{
    _nodeCountLabel        = new QLabel(tr("Node Count"), this);
    _edgeProbabilityLabel  = new QLabel(tr("Edge Probability"), this);
    _weightLoverBoundLabel = new QLabel(tr("Weight's Lover Bound"), this);
    _weightUpperBoundLabel = new QLabel(tr("Weight's Upper Bound"), this);

    _nodeCountSB = new QSpinBox(this);
    _nodeCountSB->setRange(1, 1000);
    _nodeCountSB->setValue(10);
    connect(_nodeCountSB, QOverload<int>::of(&QSpinBox::valueChanged), [this](int i){ this->_nodeCount = i; });

    _edgeProbabilityDSB = new QDoubleSpinBox(this);
    _edgeProbabilityDSB->setRange(0.00, 1.00);
    _edgeProbabilityDSB->setValue(0.5);
    _edgeProbabilityDSB->setSingleStep(0.05);
    connect(_edgeProbabilityDSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [this](double d){ this->_edgeProbability = d; });

    _weightLoverBoundDSB = new QDoubleSpinBox(this);
    _weightLoverBoundDSB->setRange(0.00, 1.00);
    _weightLoverBoundDSB->setValue(0);
    _weightLoverBoundDSB->setSingleStep(0.05);
    _weightLoverBoundDSB->setEnabled(false);
    connect(_weightLoverBoundDSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [this](double d){ this->_weightLoverBound = d; });

    _weightUpperBoundDSB = new QDoubleSpinBox(this);
    _weightUpperBoundDSB->setRange(0.00, 1.00);
    _weightUpperBoundDSB->setValue(1);
    _weightUpperBoundDSB->setSingleStep(0.05);
    _weightUpperBoundDSB->setEnabled(false);
    connect(_weightUpperBoundDSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [this](double d){ this->_weightUpperBound = d; });

    _isDirectedCB = new QCheckBox(tr("Directed"), this);
    connect(_isDirectedCB,  QOverload<int>::of(&QCheckBox::stateChanged),
            [this](int state){ _isDirecetd = (state==0) ? false : true; });

    _isWeightedCB = new QCheckBox(tr("Weighted"), this);
    connect(_isWeightedCB, QOverload<int>::of(&QCheckBox::stateChanged),
            [this](int state)
            {
                switch (state)
                {
                    case 0 :
                            this->_isWeighted = false;
                            this->_weightLoverBoundDSB->setEnabled(false);
                            this->_weightUpperBoundDSB->setEnabled(false);
                            break;
                    case 2 :
                            this->_isWeighted = true;
                            this->_weightLoverBoundDSB->setEnabled(true);
                            this->_weightUpperBoundDSB->setEnabled(true);
                            break;
                    default : break;
                }
            });

    _buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
    connect(_buttonBox, &QDialogButtonBox::accepted, this, &NetworkCreateDialog::accept);
    connect(_buttonBox, &QDialogButtonBox::rejected, this, &NetworkCreateDialog::reject);

    _mainLayout = new QGridLayout(this);
    _mainLayout->addWidget(_nodeCountLabel, 0, 0);
    _mainLayout->addWidget(_nodeCountSB, 0, 1);
    _mainLayout->addWidget(_edgeProbabilityLabel, 1, 0);
    _mainLayout->addWidget(_edgeProbabilityDSB, 1, 1);
    _mainLayout->addWidget(_isDirectedCB, 2, 0);
    _mainLayout->addWidget(_isWeightedCB, 2, 1);
    _mainLayout->addWidget(_weightLoverBoundLabel, 3, 0);
    _mainLayout->addWidget(_weightLoverBoundDSB, 3, 1);
    _mainLayout->addWidget(_weightUpperBoundLabel, 4, 0);
    _mainLayout->addWidget(_weightUpperBoundDSB, 4, 1);
    _mainLayout->addWidget(_buttonBox, 5, 0, 1, 2);

    setLayout(_mainLayout);
    setWindowTitle(tr("Graph Creator Dialog"));
    setMinimumSize(300, 300);
}

int GraphView::NetworkCreateDialog::getNodeCount() const
{
    return _nodeCount;
}

bool GraphView::NetworkCreateDialog::isDirected() const
{
    return _isDirecetd;
}

bool GraphView::NetworkCreateDialog::isWeighted() const
{
    return  _isWeighted;
}
double GraphView::NetworkCreateDialog::getEdgeProbability() const
{
    return _edgeProbability;
}

double GraphView::NetworkCreateDialog::getWeightLoverBound() const
{
    return _weightLoverBound;
}

double GraphView::NetworkCreateDialog::getWeightUpperBound() const
{
    return  _weightUpperBound;
}

GraphView::NetworkCreateDialog::~NetworkCreateDialog()
{
    delete _mainLayout;
    delete _nodeCountLabel;
    delete _edgeProbabilityLabel;
    delete _weightLoverBoundLabel;
    delete _weightUpperBoundLabel;
    delete _nodeCountSB;
    delete _weightLoverBoundDSB;
    delete _weightUpperBoundDSB;
    delete _edgeProbabilityDSB;
    delete _isDirectedCB;
    delete _isWeightedCB;
    delete _buttonBox;
}

#ifndef MODELEDGE_H
#define MODELEDGE_H

namespace GraphModel{

class Node;

class Edge
{
public:
    Edge(GraphModel::Node* source, GraphModel::Node* destination, double weight);

    GraphModel::Node*  getSourceNode() const;
    GraphModel::Node*  getDestinationNode() const;
    double             getSourceToDestinationWeight() const;
    double             getDestinationToSourceWeight() const;
    bool               isBidirectional() const;
    bool               isActive() const;
    void               activate();
    void               deactivate();
    void               setBidirectional();
    void               setDestinationToSourceWeight(double weight);

private:
    GraphModel::Node   *_source;
    GraphModel::Node   *_destination;
    double             _sourceToDestinationWeight;
    double             _destinationToSourceWeight;
    bool               _isBidirectional;
    bool               _isActive;

};

}
#endif // MODELEDGE_H

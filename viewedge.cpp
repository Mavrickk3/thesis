#include "viewedge.h"
#include "viewnode.h"

#include <qmath.h>
#include <QPainter>
#include <QDebug>

GraphView::Edge::Edge(Node &sourceNode, Node &destinationNode, bool isWeighted, double sourceToDestinationWeight,
                      bool isDirected)
: _sourceNode(sourceNode), _destinationNode(destinationNode), _isWeighted(isWeighted), _isDirected(isDirected), _isSymmetric(false),
  _isActive(false), _sourceToDestinationWeight(sourceToDestinationWeight), _destinationToSourceWeight(NO_WEIGHT),
   _colorActive(Qt::green), _colorInactive(Qt::black), _arrowSize(3), _edgeThickness(0.2), _weightBoxWidth(30),
   _weightBoxHeight(10), _weightFontSize(3)
{
    setZValue(0);
    _sourceNode.addEdge(this);
    _destinationNode.addEdge(this);
    adjust();
}

GraphView::Node& GraphView::Edge::getSourceNode() const
{
    return _sourceNode;
}

GraphView::Node& GraphView::Edge::getDestinationNode() const
{
    return _destinationNode;
}

bool GraphView::Edge::isWeighted() const
{
    return _isWeighted;
}

double GraphView::Edge::getSourceToDestinationWeight() const
{
    if(_isWeighted)
    {
        return _sourceToDestinationWeight;
    }
    else
    {
        return NO_WEIGHT;
    }
}

double GraphView::Edge::getDestinationToSourceWeight() const
{
    if(_isWeighted)
    {
        return _destinationToSourceWeight;
    }
    else
    {
        return NO_WEIGHT;
    }
}

void GraphView::Edge::setSourceToDestinationWeight(double weight)
{
    if(_isWeighted)
    {
        _sourceToDestinationWeight = weight;
    }
}

void GraphView::Edge::setDestinationToSourceWeight(double weight)
{
    if(_isWeighted)
    {
        _destinationToSourceWeight = weight;
    }
}

bool GraphView::Edge::isDirected() const
{
    return _isDirected;
}

qreal GraphView::Edge::getArrowSize() const
{
    return _arrowSize;
}

void GraphView::Edge::setArrowSize(qreal size)
{
    _arrowSize = size;
}

qreal GraphView::Edge::getEdgeThickness() const
{
    return _edgeThickness;
}

void GraphView::Edge::setEdgeThickness(qreal size)
{
    _edgeThickness = size;
}

int GraphView::Edge::getWeightFontSize() const
{
    return _weightFontSize;
}

void GraphView::Edge::setWeightFontSize(int size)
{
    _weightFontSize = size;
}

qreal GraphView::Edge::getWeightBoxWidth() const
{
    return _weightBoxWidth;
}

void GraphView::Edge::setWeightBoxWidth(qreal size)
{
    _weightBoxWidth = size;
}

qreal GraphView::Edge::getWeightBoxHeight() const
{
    return _weightBoxHeight;
}

void GraphView::Edge::setWeightBoxHeight(qreal size)
{
    _weightBoxHeight = size;
}

void GraphView::Edge::enlarge(double value)
{
    _arrowSize       *= value;
    _edgeThickness   *= value;
    _weightBoxWidth  *= value;
    _weightBoxHeight *= value;
    _weightFontSize  = static_cast<int>(_weightFontSize*value);
}

bool GraphView::Edge::isSymmetric() const
{
    if(_isDirected)
    {
        return _isSymmetric;
    }
    else
    {
       return false;
    }
}

void GraphView::Edge::setSymmetric(bool l)
{
    if(_isDirected)
    {
        if(!_isSymmetric && l)
        {
            _weightBoxHeight *= 2.0;
        }
        else if(_isSymmetric && !l)
        {
            _weightBoxHeight /= 2.0;
        }
        _isSymmetric = l;
    }
}

bool GraphView::Edge::isActive() const
{
    return _isActive;
}

void GraphView::Edge::setActive(bool l)
{
    _isActive = l;
}

QColor GraphView::Edge::getColorActive() const
{
    return _colorActive;
}

void GraphView::Edge::setColorActive(QColor color)
{
    _colorActive = color;
}

QColor GraphView::Edge::getColorInactive() const
{
    return _colorInactive;
}

void GraphView::Edge::setColorInactive(QColor color)
{
    _colorInactive = color;
}

int GraphView::Edge::type() const
{
    return Type;
}

void GraphView::Edge::adjust()
{
    QLineF line(mapFromItem(&_sourceNode, 0, 0), mapFromItem(&_destinationNode, 0, 0));
    qreal length = line.length();

    prepareGeometryChange();

    if (length > qreal(20.))
    {
        QPointF edgeOffset((line.dx() * 10) / length, (line.dy() * 10) / length);

        _sourcePoint = line.p1() + edgeOffset;
        _destPoint = line.p2() - edgeOffset;
    }
    else
    {
        _sourcePoint = _destPoint = line.p1();
    }
}

QRectF GraphView::Edge::boundingRect() const
{
    qreal penWidth = 1;
    qreal extra = (penWidth + _arrowSize) / 2.0;

    return QRectF(_sourcePoint, QSizeF(_destPoint.x() - _sourcePoint.x(), _destPoint.y() - _sourcePoint.y()))
                  .normalized().adjusted(-extra, -extra, extra, extra);
}

void GraphView::Edge::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QLineF line(_sourcePoint, _destPoint);
    if (line.length() < 5)
    {
        return;
    }

    QColor _edgeColor = _isActive ? _colorActive : _colorInactive;

    painter->setPen(QPen(_edgeColor, _edgeThickness, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter->drawLine(line);

    if(_isDirected)
    {
        double angle = std::atan2(-line.dy(), line.dx());
        painter->setBrush(_edgeColor);

        QPointF _destArrowP1 = _destPoint + QPointF(sin(angle - M_PI / 3) * _arrowSize,
                                                    cos(angle - M_PI / 3) * _arrowSize);
        QPointF _destArrowP2 = _destPoint + QPointF(sin(angle - M_PI + M_PI / 3) * _arrowSize,
                                                    cos(angle - M_PI + M_PI / 3) * _arrowSize);
        painter->drawPolygon(QPolygonF() << line.p2() << _destArrowP1 << _destArrowP2);

        if(_isSymmetric)
        {
            QLineF reverseLine(_destPoint, _sourcePoint);
            double reverseAngle = std::atan2(-reverseLine.dy(), reverseLine.dx());

            QPointF _sourceArrowP1 = _sourcePoint + QPointF(sin(reverseAngle - M_PI / 3) * _arrowSize,
                                                        cos(reverseAngle - M_PI / 3) * _arrowSize);
            QPointF _sourceArrowP2 = _sourcePoint + QPointF(sin(reverseAngle - M_PI + M_PI / 3) * _arrowSize,
                                                        cos(reverseAngle - M_PI + M_PI / 3) * _arrowSize);
            painter->drawPolygon(QPolygonF() << reverseLine.p2() << _sourceArrowP1 << _sourceArrowP2);
        }
    }

    if(_isWeighted)
    {
        QFont font = painter->font();
        font.setPointSize(_weightFontSize);
        painter->setFont(font);
        painter->setPen(Qt::black);

        QPointF middle = line.center();
        QRectF weight_rect(middle.x() - _weightBoxWidth/2.0, middle.y() - _weightBoxHeight/2.0,
                           _weightBoxWidth, _weightBoxHeight);

        int sourceNodeId      = _sourceNode.getId();
        int destinationNodeId = _destinationNode.getId();
        if(_isDirected)
        {
            QString sourceToDestinationWeightString(QString::number(sourceNodeId) + "->" +
                                                    QString::number(destinationNodeId) + ": " +
                                                    QString::number(_sourceToDestinationWeight));
            if(_isSymmetric)
            {
                QString destinationToSourceWeightString(QString::number(destinationNodeId) + "->" +
                                                        QString::number(sourceNodeId) + ": " +
                                                        QString::number(_destinationToSourceWeight));
                painter->drawText(weight_rect, Qt::AlignCenter, sourceToDestinationWeightString +
                                                                QString('\n') +
                                                                destinationToSourceWeightString);
            }
            else
            {
                painter->drawText(weight_rect, Qt::AlignCenter, sourceToDestinationWeightString);
            }
        }
        else
        {
            QString weightString(QString::number(sourceNodeId) + "<->" +
                                 QString::number(destinationNodeId) + ": " +
                                 QString::number(_sourceToDestinationWeight));
            painter->drawText(weight_rect, Qt::AlignCenter, weightString);
        }
    }
}

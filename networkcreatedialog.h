#ifndef NETWORKCREATEDIALOG_H
#define NETWORKCREATEDIALOG_H

#include <QDialog>
#include <QLayout>
#include <QLabel>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QSpinBox>

namespace GraphView {

class NetworkCreateDialog : public QDialog
{
public:
    NetworkCreateDialog(QWidget *parent = nullptr);
    ~NetworkCreateDialog();

    int    getNodeCount() const;
    bool   isDirected() const;
    bool   isWeighted() const;
    double getEdgeProbability() const;
    double getWeightLoverBound() const;
    double getWeightUpperBound() const;

private:
    QGridLayout      *_mainLayout;
    QLabel           *_nodeCountLabel;
    QLabel           *_edgeProbabilityLabel;
    QLabel           *_weightLoverBoundLabel;
    QLabel           *_weightUpperBoundLabel;
    QSpinBox         *_nodeCountSB;
    QDoubleSpinBox   *_edgeProbabilityDSB;
    QDoubleSpinBox   *_weightLoverBoundDSB;
    QDoubleSpinBox   *_weightUpperBoundDSB;
    QCheckBox        *_isDirectedCB;
    QCheckBox        *_isWeightedCB;
    QDialogButtonBox *_buttonBox;
    int              _nodeCount;
    bool             _isDirecetd;
    bool             _isWeighted;
    double           _edgeProbability;
    double           _weightLoverBound;
    double           _weightUpperBound;

};

}

#endif // NETWORKCREATEDIALOG_H

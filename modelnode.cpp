#include "modelnode.h"

GraphModel::Node::Node(int id) : _id(id), _isActive(false) {}

int GraphModel::Node::getId() const
{
    return _id;
}

bool GraphModel::Node::isActive() const
{
    return _isActive;
}

void GraphModel::Node::activate()
{
    _isActive = true;
}

void GraphModel::Node::deactivate()
{
    _isActive = false;
}

bool GraphModel::Node::operator ==(const GraphModel::Node &other) const
{
    return _id == other._id;
}

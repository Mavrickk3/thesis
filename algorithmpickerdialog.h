#ifndef ALGORITHMPICKERDIALOG_H
#define ALGORITHMPICKERDIALOG_H

#include <QDialog>
#include <QLayout>
#include <QLabel>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QListWidget>
#include <QDialogButtonBox>

#include "model.h"

namespace GraphView {

class AlgorithmPickerDialog : public QDialog
{
public:
    AlgorithmPickerDialog(const QVector<int> &nodes, QWidget *parent = nullptr);
    ~AlgorithmPickerDialog();

    int                       getSelectedNodesCount() const;
    void                      modifyNodesVectorForSelectedNodesId(QVector<int>& selectedNodes);
    GraphModel::AlgorithmType getSelectedAlgorithm() const;
    double                    getTresholdForDecisionBasedAlgorithm() const;

private:
    QGridLayout               *_mainLayout;
    QComboBox                 *_comboBox;
    QLabel                    *_tresholdLabel;
    QDoubleSpinBox            *_tresholdDSB;
    QLabel                    *_initiallyActiveNodesLabel;
    QListWidget               *_listWidget;
    QDialogButtonBox          *_buttonBox;
    double                    _tresholdForDecisionBasedAlgorithm;
    GraphModel::AlgorithmType _selectedAlgorithm;

};

}
#endif // ALGORITHMPICKERDIALOG_H

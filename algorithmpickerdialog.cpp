#include "algorithmpickerdialog.h"

GraphView::AlgorithmPickerDialog::AlgorithmPickerDialog(const QVector<int> &nodes, QWidget *parent)
    : QDialog(parent)
{
    _comboBox = new QComboBox(this);
    _comboBox->addItem("Decision Based Algorithm");
    _comboBox->addItem("Independent Cascade Algorithm");

    _tresholdLabel = new QLabel(tr("Treshold for Decision Based Algorithm"));

    _tresholdDSB = new QDoubleSpinBox(this);
    _tresholdDSB->setRange(0.00, 1.00);
    _tresholdDSB->setValue(0.5);
    _tresholdDSB->setSingleStep(0.05);

    _initiallyActiveNodesLabel = new QLabel(tr("Initially active nodes"), this);

    _listWidget = new QListWidget(this);
    _listWidget->setSelectionMode(QAbstractItemView::SelectionMode::MultiSelection);

    for(auto node : nodes)
    {
        _listWidget->addItem(QString::number(node));
    }

    _buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);

    connect(_comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
        [=](int index)
        {
            _selectedAlgorithm = static_cast<GraphModel::AlgorithmType>(index);
            switch(_selectedAlgorithm)
            {
                case GraphModel::AlgorithmType::DECISION_BASED:
                    _tresholdLabel->show();
                    _tresholdDSB->show();
                    break;
                case GraphModel::AlgorithmType::INDEPENDENT_CASCADE:
                    _tresholdLabel->hide();
                    _tresholdDSB->hide();
                    break;
                default:
                    break;
            }
        });
    _comboBox->currentIndexChanged(_comboBox->currentIndex());

    connect(_tresholdDSB, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            [this](double d){ this->_tresholdForDecisionBasedAlgorithm = d; });
    _tresholdDSB->valueChanged(_tresholdDSB->value());

    connect(_buttonBox, &QDialogButtonBox::accepted, this, &AlgorithmPickerDialog::accept);
    connect(_buttonBox, &QDialogButtonBox::rejected, this, &AlgorithmPickerDialog::reject);

    _mainLayout = new QGridLayout(this);
    _mainLayout->addWidget(_comboBox, 0, 0, 1, 2);
    _mainLayout->addWidget(_tresholdLabel, 1, 0);
    _mainLayout->addWidget(_tresholdDSB, 1, 1);
    _mainLayout->addWidget(_initiallyActiveNodesLabel, 2, 0, 1, 2);
    _mainLayout->addWidget(_listWidget, 3, 0, 1, 2);
    _mainLayout->addWidget(_buttonBox, 4, 0, 1, 2);

    setLayout(_mainLayout);
    setWindowTitle(tr("Algorithm Picker Dialog"));
    setMinimumSize(300, 300);
}

GraphView::AlgorithmPickerDialog::~AlgorithmPickerDialog()
{
    delete _buttonBox;
    delete _listWidget;
    delete _initiallyActiveNodesLabel;
    delete _tresholdDSB;
    delete _tresholdLabel;
    delete _comboBox;
    delete _mainLayout;
}

int GraphView::AlgorithmPickerDialog::getSelectedNodesCount() const
{
    return _listWidget->selectedItems().size();
}

void GraphView::AlgorithmPickerDialog::modifyNodesVectorForSelectedNodesId(QVector<int> &selectedNodes)
{
    QList<QListWidgetItem*> selectedItems = _listWidget->selectedItems();

    selectedNodes.resize(selectedItems.size());
    for(int i = 0; i < selectedNodes.size(); ++i)
    {
        selectedNodes[i] = selectedItems[i]->text().toInt();
    }
}

GraphModel::AlgorithmType GraphView::AlgorithmPickerDialog::getSelectedAlgorithm() const
{
    return _selectedAlgorithm;
}

double GraphView::AlgorithmPickerDialog::getTresholdForDecisionBasedAlgorithm() const
{
    return _tresholdForDecisionBasedAlgorithm;
}

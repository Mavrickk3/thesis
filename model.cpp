#include <QFile>
#include <QRandomGenerator>

#include "model.h"
#include "modelnode.h"
#include "modeledge.h"

GraphModel::Model::Model(QObject *parent): QObject(parent), _isDirected(false), _isWeighted(false),
 _currentAlgorithm(GraphModel::AlgorithmType::NO_ALGORITHM), _isAlgorithmInProcess(false), _isConverged(false) {}

GraphModel::Model::~Model()
{
    deleteGraph(_edges, _nodes);
}

void GraphModel::Model::createRandomGraph(int nodesCount, double edgeProb, bool directed, bool weighted,
                                          double weightsLoverBound, double weightsUpperBound)
{
    takeCareModelStateWhenLeavingAlgorithmMode();
    deleteGraph(_edges, _nodes);

    _isDirected = directed;
    _isWeighted = weighted;

    _nodes.resize(nodesCount);
    for(int i = 0; i < nodesCount; ++i)
    {
        _nodes[i] = new GraphModel::Node(i+1);
    }

    std::uniform_real_distribution<> dist(weightsLoverBound, weightsUpperBound);

    for(int i = 0; i < nodesCount; ++i)
    {
        for(int j = (_isDirected ? 0 : i+1); j < nodesCount; ++j)
        {
            if(QRandomGenerator::global()->generateDouble() < edgeProb)
            {
                double weight = _isWeighted
                                 ? dist(*QRandomGenerator::global())
                                 : NO_WEIGHT;

                if(_isDirected && i != j)
                {
                    addNewOrUpdateExistingEdgeToBidirectional(_nodes[i], _nodes[j], &_edges, weight);
                }
                else if(!_isDirected)
                {
                    _edges.push_back(new GraphModel::Edge(_nodes[i], _nodes[j], weight));
                }
            }
        }
    }

    emit graphCreated(&_nodes, &_edges, _isDirected, _isWeighted);
}

void GraphModel::Model::createGraphFromFile(const QString &fileName)
{
    QFile inputFile(fileName);

    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       QString     line;
       QString     commentSign = "#";
       QString     errorMsg;

       movingThroughCommentAndEmptyLines(in, line, commentSign);

       // Reading first non-comment line
       QString isDirectedString = line;
       bool    isDirected;
       if(!line.isNull() && isDirectedString == QString("DIRECTED"))
       {
          isDirected = true;
       }
       else if(!line.isNull() && isDirectedString == QString("UNDIRECTED"))
       {
           isDirected = false;
       }
       else
       {
          propogateErrorMessageAndDeleteGraph("The first non-comment line of file "
                                              "must be \"DIRECTED\" or \"UNDIRECTED\"!", nullptr, nullptr);
          return;
       }

       movingThroughCommentAndEmptyLines(in, line, commentSign);

       // Reading second non-comment line
       QString isWeightedString = line;
       bool    isWeighted;
       if(!line.isNull() && isWeightedString == QString("WEIGHTED"))
       {
          isWeighted = true;
       }
       else if(!line.isNull() && isWeightedString == QString("UNWEIGHTED"))
       {
           isWeighted = false;
       }
       else
       {
          propogateErrorMessageAndDeleteGraph("The second non-comment line of file must be "
                                              "\"WEIGHTED\" or \"UNWEIGHTED\"!", nullptr, nullptr);
          return;
       }

       movingThroughCommentAndEmptyLines(in, line, commentSign);

       // Reading third non-comment line
       bool ok;
       int matrixSize = line.toInt(&ok);
       if(line.isNull() || !ok || matrixSize <= 0)
       {
           propogateErrorMessageAndDeleteGraph("The third non-comment line of file must be "
                                               "a non-negative integer!", nullptr, nullptr);
           return;
       }

       movingThroughCommentAndEmptyLines(in, line, commentSign);

       QVector<GraphModel::Node*> nodes(matrixSize);
       for(int i = 0; i < matrixSize; ++i)
       {
           nodes[i] = new Node(i+1);
       }

       QVector<GraphModel::Edge*> edges(0);

       QStringList     rowStringList;
       double          numDbl;
       double          integralPartOfNumDbl;
       int             numInt;
       int             currentLine = 1;

       while(!line.isNull() && currentLine <= matrixSize)
       {
           rowStringList = line.split(QRegExp("\\s+"));
           if((isDirected && rowStringList.size() != matrixSize ) ||
              (!isDirected && rowStringList.size() != currentLine) )
           {
               propogateErrorMessageAndDeleteGraph("Error in row:" + QString::number(currentLine) + "! "
                                                   "Size of matrix's row is not appropiate!", &edges, &nodes);
               return;
           }

           for(int j = 0; j < rowStringList.size(); ++j)
           {
               if(isWeighted)
               {
                    if(j+1 == currentLine && (numInt = rowStringList[j].toInt()) != -1)
                    {
                        propogateErrorMessageAndDeleteGraph("Error in row:" + QString::number(currentLine) +
                                                            ", column:" + QString::number(j+1) + "! "
                                                            "Diagonal of weighted graph must contain -1 only!",
                                                             &edges, &nodes);
                        return;
                    }
                    else if(j+1 != currentLine)
                    {
                          integralPartOfNumDbl = 0;
                          if(((numDbl = rowStringList[j].toDouble(&ok)) < 0 || numDbl > 1 || !ok) &&
                                (numDbl >= 0 || std::modf(numDbl, &integralPartOfNumDbl) != 0 ||
                                static_cast<int>(integralPartOfNumDbl) != -1))
                          {
                                  propogateErrorMessageAndDeleteGraph("Error in row:" + QString::number(currentLine) +
                                                                      ", column:" + QString::number(j+1) + "! "
                                                                      "Weighted graph must contain numbers "
                                                                      "between 0 and 1 or -1 if there is no edge"
                                                                      "or we are in the diagonal!",  &edges, &nodes);
                                  return;
                          }

                          if(static_cast<int>(integralPartOfNumDbl) != -1)
                          {
                              addNewOrUpdateExistingEdgeToBidirectionalDuringFileReading
                                      (nodes[currentLine-1], nodes[j], &edges, numDbl, isDirected);

                          }
                    }
               }
               else if(!isWeighted)
               {
                   if(j+1 == currentLine)
                   {
                        numInt = rowStringList[j].toInt(&ok);
                        if(!ok || numInt != 0)
                        {
                            propogateErrorMessageAndDeleteGraph("Error in row:" + QString::number(currentLine) +
                                                                ", column:" + QString::number(j+1) + "! "
                                                                "Diagonal of unweighted graph must contain 0 only!",
                                                                &edges, &nodes);
                            return;
                        }
                    }
                    else if(j+1 != currentLine)
                    {
                        numInt = rowStringList[j].toInt(&ok);
                        if(!ok || (numInt != 0 && numInt != 1))
                        {
                            propogateErrorMessageAndDeleteGraph("Error in row:" + QString::number(currentLine) +
                                                                ", column:" + QString::number(j+1) + "! "
                                                                "Unweighted graph must contain "
                                                                "0 or 1 only(Except in diagonal)!", &edges, &nodes);
                            return;
                        }

                        if(numInt != 0)
                        {
                            addNewOrUpdateExistingEdgeToBidirectionalDuringFileReading
                                (nodes[currentLine-1], nodes[j], &edges, GraphModel::Model::NO_WEIGHT, isDirected);
                        }
                    }
                }
           }
           ++currentLine;

           movingThroughCommentAndEmptyLines(in, line, commentSign);
       }

       if(!line.isNull() || currentLine <= matrixSize)
       {
           propogateErrorMessageAndDeleteGraph("Size of matrix is not appropiate!", &edges, &nodes);
           return;
       }

       inputFile.close();

       takeCareModelStateWhenLeavingAlgorithmMode();
       deleteGraph(_edges, _nodes);

       _isDirected = isDirected;
       _isWeighted = isWeighted;

       _nodes = nodes;
       _edges = edges;

       emit graphCreated(&_nodes, &_edges, _isDirected, _isWeighted);
    }
    else
    {
        propogateErrorMessageAndDeleteGraph("Reading graph from \"" + fileName + "\" has failed. "
                                            "The file cannot be opened!", nullptr, nullptr);
    }
}

bool GraphModel::Model::saveStateIntoFile(QString &fileName) const
{
    if(_currentAlgorithm == GraphModel::AlgorithmType::NO_ALGORITHM)
    {
        return false;
    }

    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);

        QString graphType = "";
        graphType += _isDirected ? "Directed" : "Undirected";
        graphType += " - ";
        graphType += _isWeighted ? "Weighted" : "Unweighted";
        stream << "Graph Type: " << graphType << endl;

        int nodeCount = _nodes.size();
        stream << "Count of Nodes: " << QString::number(nodeCount) << endl;

        QString algoTypeString = (_currentAlgorithm == GraphModel::AlgorithmType::DECISION_BASED) ?
                                     "Decision Based" : "Independent Cascade";
        stream << "Algorithm Type: " << algoTypeString << endl;

        if(_currentAlgorithm == GraphModel::AlgorithmType::DECISION_BASED)
        {
            stream << "Global treshold: " << _tresholdForDecisionBasedAlgorithm << endl;
        }

        for(int i = 0; i < _activatedNodesPerRound.size(); ++i)
        {
            if(i == 0)
            {
                stream << endl << "Initially active nodes:" << endl;
            }
            else if(std::get<0>(_activatedNodesPerRound[i]) != std::get<0>(_activatedNodesPerRound[i-1]))
            {
                stream << endl << "Activated nodes in " << std::get<0>(_activatedNodesPerRound[i])
                       << ". iteration:" << endl;
            }

            int node = std::get<1>(_activatedNodesPerRound[i]);
            stream << "- Node: " << QString::number(node) << endl;

            int activatedBy = std::get<2>(_activatedNodesPerRound[i]);
            if(activatedBy != -1)
            {
                switch(_currentAlgorithm)
                {
                case GraphModel::AlgorithmType::DECISION_BASED:
                    stream << "-- Active neighbours count: " << QString::number(activatedBy) << endl;
                    break;
                case GraphModel::AlgorithmType::INDEPENDENT_CASCADE:
                    stream << "-- Node that activated it : " << QString::number(activatedBy) << endl;
                    break;
                default: break;
                }
            }

            double activatingWeight = std::get<3>(_activatedNodesPerRound[i]);
            if(static_cast<int>(activatingWeight) != -1)
            {
                switch(_currentAlgorithm)
                {
                case GraphModel::AlgorithmType::DECISION_BASED:
                    stream << "-- Ratio of active/inactive nodes: " << QString::number(activatingWeight) << endl;
                    break;
                case GraphModel::AlgorithmType::INDEPENDENT_CASCADE:
                    stream << "-- Activating weight: " << QString::number(activatingWeight) << endl;
                    break;
                default: break;
                }
            }
        }

        if(_isConverged)
        {
            stream << endl << "The algorithm was converged!" << endl;
        }

        file.close();
        return true;
    }

    return false;
}

bool GraphModel::Model::startAlgorithm(GraphModel::AlgorithmType algoType, const QVector<int> &initActiveNodes,
                                       double treshold)
{    
    if(GraphModel::AlgorithmType::DECISION_BASED > algoType ||
       GraphModel::AlgorithmType::INDEPENDENT_CASCADE < algoType)
    {
        emit errorMessageFromModel("The given algorithm is not found between the playable algorithm types!");
        return false;
    }

    if(initActiveNodes.size() == 0 || initActiveNodes.size() == _nodes.size())
    {
        emit errorMessageFromModel("You must select at least 1 node, but not all of them!");
        return false;
    }

    if(algoType == GraphModel::AlgorithmType::DECISION_BASED && (_isDirected || _isWeighted))
    {
        emit errorMessageFromModel("You can play Decison Based Algorithm "
                                   "only if graph is neither directed nor weighted!");
        return false;
    }
    else if(algoType == GraphModel::AlgorithmType::INDEPENDENT_CASCADE && (!_isDirected || !_isWeighted))
    {
        emit errorMessageFromModel("You can play Independent Cascade Algorithm "
                                   "only if graph is both directed and weighted!");
        return false;
    }

    takeCareModelStateWhenLeavingAlgorithmMode();

    _currentAlgorithm     = algoType;
    _isAlgorithmInProcess = true;
    _isConverged          = false;

    if(_currentAlgorithm == GraphModel::AlgorithmType::DECISION_BASED)
    {
        _tresholdForDecisionBasedAlgorithm = treshold;
    }

    for(auto modelNode : _nodes)
    {
        for(auto viewActiveNode : initActiveNodes)
        {
            if(modelNode->getId() == viewActiveNode)
            {
                modelNode->activate();
                if(algoType == GraphModel::AlgorithmType::DECISION_BASED)
                {
                    _previouslyActivatedNodes.push_back(modelNode);
                }
                _activatedNodesPerRound.push_back(std::make_tuple<int, int, int, double>
                                                  (0, modelNode->getId(), -1, -1));
                break;
            }
        }
    }

    ++_iterationCount;
    emit graphModified(&_nodes, &_edges);
    return true;
}

void GraphModel::Model::stepAlgorithm()
{
    if(_isConverged)
    {
        return;
    }

    if(_currentAlgorithm == GraphModel::AlgorithmType::DECISION_BASED)
    {
        QVector<GraphModel::Node*> newlyActivatedNodes;

        for(auto currentNode : _nodes)
        {
            if(!(currentNode->isActive()))
            {
                int allNeighborCount = 0;
                int activeNeighborCount = 0;

                for(auto edge : _edges)
                {
                    auto srcNode = edge->getSourceNode();
                    auto dstNode = edge->getDestinationNode();

                    GraphModel::Node* other;

                    if(*srcNode == *currentNode || *dstNode == *currentNode)
                    {
                        other = (*srcNode == *currentNode) ? dstNode : srcNode;

                        if(std::find_if(_previouslyActivatedNodes.begin(), _previouslyActivatedNodes.end(),
                                        [other](GraphModel::Node* n)
                                        {
                                            return n->getId() == other->getId();
                                        }) != _previouslyActivatedNodes.end())
                        {
                            ++activeNeighborCount;
                        }

                        ++allNeighborCount;
                    }
                }

                double activeInactiveRatio = (static_cast<double>(activeNeighborCount) /
                                               static_cast<double>(allNeighborCount));
                if(allNeighborCount > 0 && activeInactiveRatio > _tresholdForDecisionBasedAlgorithm)
                {
                    currentNode->activate();
                    newlyActivatedNodes.push_back(currentNode);
                    _activatedNodesPerRound.push_back(std::make_tuple<int, int, int, double>
                                                     ((const int)_iterationCount, currentNode->getId(),
                                                      (const int)activeNeighborCount,
                                                      (const double)activeInactiveRatio));
                }
            }
        }

        _isConverged = (newlyActivatedNodes.size() == 0);
        if(!_isConverged)
        {
            _previouslyActivatedNodes.append(newlyActivatedNodes);
        }

        emit graphModified(&_nodes, nullptr);
    }
    else if(_currentAlgorithm == GraphModel::AlgorithmType::INDEPENDENT_CASCADE)
    {
        QVector<GraphModel::Edge*> potentialEdges;
        QVector<GraphModel::Node*> activeNodesFromThisRound;

        for(auto edge : _edges)
        {
            auto srcOfEdge = edge->getSourceNode();
            auto dstOfEdge = edge->getDestinationNode();

            if((!(srcOfEdge->isActive()) && dstOfEdge->isActive() && edge->isBidirectional()) &&
                std::find_if(_previouslyActivatedNodes.begin(), _previouslyActivatedNodes.end(),
                             [dstOfEdge](auto n)
                             {
                                 return *n == *dstOfEdge;
                             }) == _previouslyActivatedNodes.end())
            {
                potentialEdges.push_back(edge);

                if(std::find_if(activeNodesFromThisRound.begin(), activeNodesFromThisRound.end(),
                                [dstOfEdge](auto n) { return *n == *dstOfEdge; })
                   == activeNodesFromThisRound.end())
                {
                    activeNodesFromThisRound.push_back(dstOfEdge);
                }
            }
            else if(srcOfEdge->isActive()  && !(dstOfEdge->isActive()) &&
                    std::find_if(_previouslyActivatedNodes.begin(), _previouslyActivatedNodes.end(),
                                 [srcOfEdge](auto n)
                                 {
                                     return *n == *srcOfEdge;
                                 }) == _previouslyActivatedNodes.end())
            {
                potentialEdges.push_back(edge);

                if(std::find_if(activeNodesFromThisRound.begin(), activeNodesFromThisRound.end(),
                                [srcOfEdge](auto n) { return *n == *srcOfEdge; })
                   == activeNodesFromThisRound.end())
                {
                    activeNodesFromThisRound.push_back(srcOfEdge);
                }
            }
        }

        _previouslyActivatedNodes.append(activeNodesFromThisRound);

        std::uniform_real_distribution<> dist(0, 1);
        int newlyActivatedNodes = 0;

        for(auto edge : potentialEdges)
        {
            double random = dist(*QRandomGenerator::global());

            auto srcOfEdge = edge->getSourceNode();
            auto dstOfEdge = edge->getDestinationNode();

            if(srcOfEdge->isActive() && !(dstOfEdge->isActive()) &&
               random < edge->getSourceToDestinationWeight())
            {
                dstOfEdge->activate();
                edge->activate();

                ++newlyActivatedNodes;

                _activatedNodesPerRound.push_back(std::make_tuple<int, int, int, double>
                                                  ((const int)_iterationCount, dstOfEdge->getId(),
                                                   srcOfEdge->getId(), edge->getSourceToDestinationWeight()));
            }
            else if(!(srcOfEdge->isActive()) && dstOfEdge->isActive() &&
                    random < edge->getDestinationToSourceWeight())
            {
                srcOfEdge->activate();
                edge->activate();

                ++newlyActivatedNodes;

                _activatedNodesPerRound.push_back(std::make_tuple<int, int, int, double>
                                                  ((const int)_iterationCount, srcOfEdge->getId(),
                                                   dstOfEdge->getId(), edge->getDestinationToSourceWeight()));
            }
        }

        _isConverged = (newlyActivatedNodes == 0);
        emit graphModified(&_nodes, &_edges);
    }

    ++_iterationCount;
}

void GraphModel::Model::endAlgorithm()
{
    takeCareModelStateWhenLeavingAlgorithmMode();
    emit graphModified(&_nodes, &_edges);
}

bool GraphModel::Model::isDirected() const
{
    return _isDirected;
}

bool GraphModel::Model::isWeighted() const
{
    return _isWeighted;
}

bool GraphModel::Model::isAlgorithmInProcess() const
{
    return _isAlgorithmInProcess;
}

bool GraphModel::Model::isConverged() const
{
    return _isConverged;
}

void GraphModel::Model::movingThroughCommentAndEmptyLines(QTextStream &in, QString &line, const QString &commentSign)
{
    do
    {
        line = in.readLine().simplified();
    }
    while(!line.isNull() && ( line.startsWith(commentSign) || line == "" ) );
}

void GraphModel::Model::propogateErrorMessageAndDeleteGraph(const QString &msg, QVector<GraphModel::Edge*>* edges,
                                                                                QVector<GraphModel::Node*>* nodes)
{
    QString preFix = "Error while parsing the file! ";

    emit errorMessageFromModel(preFix + msg);

    if(edges != nullptr)
    {
        deleteEdges(*edges);
    }
    if(nodes != nullptr)
    {
        deleteNodes(*nodes);
    }
}

QPair<bool,int> GraphModel::Model::checkReverseEdgeExistence(GraphModel::Node* const src,
                                                             GraphModel::Node* const dst,
                                                             QVector<GraphModel::Edge*> const *edges) const
{
    bool reverseEdgeExists = false;
    int i = 0;

    for(; i < edges->size(); ++i)
    {
        Node* srcToCheck = (*edges)[i]->getSourceNode();
        Node* dstToCheck = (*edges)[i]->getDestinationNode();

        if(*srcToCheck == *dst && *dstToCheck == *src)
        {
            reverseEdgeExists = true;
            break;
        }
    }

    return QPair<bool,int>(reverseEdgeExists, i);
}

void GraphModel::Model::addNewOrUpdateExistingEdgeToBidirectional
(GraphModel::Node *src, GraphModel::Node *dst, QVector<GraphModel::Edge *> *edges, double weight)
{
    QPair<bool,int> reverseEdgeExistsAndIndex = checkReverseEdgeExistence(src, dst, edges);

    if(reverseEdgeExistsAndIndex.first)
    {
        (*edges)[reverseEdgeExistsAndIndex.second]->setBidirectional();
        (*edges)[reverseEdgeExistsAndIndex.second]->setDestinationToSourceWeight(weight);
    }
    else
    {
        (*edges).push_back(new GraphModel::Edge(src, dst, weight));
    }
}

void GraphModel::Model::addNewOrUpdateExistingEdgeToBidirectionalDuringFileReading
(GraphModel::Node *src, GraphModel::Node *dst,
 QVector<GraphModel::Edge*> *edges, double weight, bool isDirected)
{
    if(isDirected)
    {
        addNewOrUpdateExistingEdgeToBidirectional(src, dst, edges, weight);
    }
    else
    {
        (*edges).push_back(new GraphModel::Edge(src, dst, weight));
    }
}

void GraphModel::Model::takeCareModelStateWhenLeavingAlgorithmMode()
{
    _currentAlgorithm     = GraphModel::AlgorithmType::NO_ALGORITHM;
    _isAlgorithmInProcess = false;
    _previouslyActivatedNodes.clear();

    for(auto edge : _edges)
    {
        if(edge->isActive())
        {
            edge->deactivate();
        }
    }

    for(auto node : _nodes)
    {
        if(node->isActive())
        {
            node->deactivate();
        }
    }

    _activatedNodesPerRound.clear();
    _iterationCount = 0;
}

void GraphModel::Model::deleteEdges(QVector<GraphModel::Edge*> &edges)
{
    for(auto edge : edges)
    {
        delete edge;
    }
    edges.clear();
}

void GraphModel::Model::deleteNodes(QVector<GraphModel::Node*> &nodes)
{
    for(auto node : nodes)
    {
        delete node;
    }
    nodes.clear();
}

void GraphModel::Model::deleteGraph(QVector<GraphModel::Edge*> &edges, QVector<GraphModel::Node*> &nodes)
{
    deleteEdges(edges);
    deleteNodes(nodes);
}

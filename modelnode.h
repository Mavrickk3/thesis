#ifndef MODELNODE_H
#define MODELNODE_H

namespace GraphModel{

class Node
{
public:
    Node(int id);

    int  getId() const;
    bool isActive() const;
    void activate();
    void deactivate();

    bool operator ==(const GraphModel::Node& other) const;

private:
    int  _id;
    bool _isActive;

};

}
#endif // MODELNODE_H

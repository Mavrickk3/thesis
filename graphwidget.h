#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QGraphicsView>
#include <QMenuBar>

#include "viewedge.h"
#include "viewnode.h"
#include "model.h"

namespace GraphView {

class GraphWidget : public QGraphicsView
{
    Q_OBJECT
public:
    GraphWidget(QWidget &parent, GraphModel::Model &model);
    ~GraphWidget() override;

    void itemMoved();
    bool anyNodeIsPressingByMouse() const;

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void timerEvent(QTimerEvent *event) override;
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *event) override;
#endif
    void resizeEvent(QResizeEvent *event) override;
    void drawBackground(QPainter *painter, const QRectF &rect) override;
    void scaleView(qreal scaleFactor);

private:
    GraphModel::Model         &_model;
    QMenuBar                  &_mainMenuBar;
    QVector<GraphView::Node*> _nodes;
    QVector<GraphView::Edge*> _edges;
    QMenu                     *_networkMenu;
    QMenu                     *_optionsMenu;
    QMenu                     *_edgeSubmenu;
    QMenu                     *_nodeSubmenu;
    QMenu                     *_backgroundSubmenu;
    QMenu                     *_algorithmsMenu;
    QAction                   *_createRandomGraphAction;
    QAction                   *_readGraphFromFileAction;
    QAction                   *_saveGraphStateAction;
    QAction                   *_setEdgeColorActiveAction;
    QAction                   *_setEdgeColorInactiveAction;
    QAction                   *_setArrowSizeAction;
    QAction                   *_setNodeColorActiveTopGradientAction;
    QAction                   *_setNodeColorActiveBottomGradientAction;
    QAction                   *_setNodeColorInactiveTopGradientAction;
    QAction                   *_setNodeColorInactiveBottomGradientAction;
    QAction                   *_setNodeShadowAction;
    QAction                   *_setNodeForcesAction;
    QAction                   *_shuffleAction;
    QAction                   *_setBackgroundColorTopGradientAction;
    QAction                   *_setBackgroundColorBottomGradientAction;
    QAction                   *_setEdgeThicknessAction;
    QAction                   *_invokeAlgorithmAction;
    QAction                   *_stepAlgorithmAction;
    QAction                   *_leaveAlgorithmAction;
    QColor                    _backgroundColorTopGradient;
    QColor                    _backgroundColorBottomGradient;
    int                       _timerId;

    void createActions();
    void createMenus();
    void deleteNodes();
    void deleteEdges();

public slots:
    void createRandomGraph();
    void readGraphFromFile();
    void saveStateOfGraph();
    void setEdgeColor();
    void setArrowSize();
    void setEdgeThickness();
    void setNodeColor();
    void setNodeShadow();
    void setNodeForces();
    void shuffle();
    void setBackgroundColor();
    void invokeAlgoithmPicker();
    void invokeAlgorithmStepper();
    void invokeAlgorithmLeaver();

    void zoomIn();
    void zoomOut();

private slots:
    void graphCreated(const QVector<GraphModel::Node*>*, const QVector<GraphModel::Edge*>*, bool, bool);
    void graphModified(const QVector<GraphModel::Node*>*, const QVector<GraphModel::Edge*>*);
    void showMessage(const QString &msg);

};

}

#endif // GRAPHWIDGET_H

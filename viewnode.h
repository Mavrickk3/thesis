#ifndef VIEWNODE_H
#define VIEWNODE_H

#include <QGraphicsItem>

QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

namespace GraphView {

class Edge;
class GraphWidget;

class Node : public QGraphicsItem
{
public:
    Node(GraphWidget &graphWidget, int id);

    enum         { Type = UserType + 1 };

    void         addEdge(Edge *edge);
    QList<Edge*> edges() const;
    int          getId() const;
    void         setForces(bool l);
    void         setShadow(bool l);
    bool         isActive() const;
    void         setActive(bool l);
    void         setActiveColorTopGradient(QColor color);
    void         setActiveColorBottomGradient(QColor color);
    void         setInactiveColorTopGradient(QColor color);
    void         setInactiveColorBottomGradient(QColor color);

    bool         isMousePressing() const;
    void         calculateForces();
    bool         advancePosition();
    int          type() const override { return Type; }
    QRectF       boundingRect() const override;
    QPainterPath shape() const override;
    void         paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

protected:
    QVariant     itemChange(GraphicsItemChange change, const QVariant &value) override;
    void         mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void         mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
    QList<Edge*> _edgeList;
    QPointF      _newPos;
    GraphWidget  &_graph;
    int          _id;
    bool         _forces;
    bool         _shadow;
    bool         _isActive;
    QColor       _colorActiveTopGradient;
    QColor       _colorActiveBottomGradient;
    QColor       _colorInactiveTopGradient;
    QColor       _colorInactiveBottomGradient;
    bool         _isMousePressing;
};

}

#endif // VIEWNODE_H

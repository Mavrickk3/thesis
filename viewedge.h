#ifndef VIEWEDGE_H
#define VIEWEDGE_H

#include <QGraphicsItem>

namespace GraphView {

class Node;

class Edge : public QGraphicsItem
{
public:
    Edge(Node &sourceNode, Node &destinationNode, bool isWeighted = false, double sourceToDestinationWeight = NO_WEIGHT,
         bool isDirected = false);
    Edge(const Edge&) = delete;

    enum { Type = UserType + 2, NO_WEIGHT = 0 };

    Node   &getSourceNode() const;
    Node   &getDestinationNode() const;
    double getSourceToDestinationWeight() const;
    double getDestinationToSourceWeight() const;
    bool   isDirected() const;
    bool   isWeighted() const;
    bool   isSymmetric() const;
    bool   isActive() const;
    QColor getColorInactive() const;
    QColor getColorActive() const;
    qreal  getArrowSize() const;
    qreal  getEdgeThickness() const;
    qreal  getWeightBoxWidth() const;
    qreal  getWeightBoxHeight() const;
    int    getWeightFontSize() const;
    void   setSourceToDestinationWeight(double weight);
    void   setDestinationToSourceWeight(double weight);
    void   setSymmetric(bool l);
    void   setActive(bool l);
    void   setColorInactive(QColor color);
    void   setColorActive(QColor color);
    void   setArrowSize(qreal size);
    void   setEdgeThickness(qreal size);
    void   setWeightFontSize(int size);
    void   setWeightBoxWidth(qreal size);
    void   setWeightBoxHeight(qreal size);
    void   enlarge(double value);

    void    adjust();
    int     type() const override;

protected:
    QRectF  boundingRect() const override;
    void    paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    Node    &_sourceNode;
    Node    &_destinationNode;
    QPointF _sourcePoint;
    QPointF _destPoint;
    bool    _isWeighted;
    bool    _isDirected;
    bool    _isSymmetric;
    bool    _isActive;
    double  _sourceToDestinationWeight;
    double  _destinationToSourceWeight;
    QColor  _colorActive;
    QColor  _colorInactive;
    qreal   _arrowSize;
    qreal   _edgeThickness;
    qreal   _weightBoxWidth;
    qreal   _weightBoxHeight;
    int     _weightFontSize;
};

}

#endif // VIEWEDGE_H
